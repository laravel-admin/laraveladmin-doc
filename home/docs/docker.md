# 环境

> 提供基于laravel的docker线上环境,及开发环境(php mysql nginx redis phpswoole).启动时已自动配置好并启动了队列,定时任务,进程守护程序

**相关文档**

[docker文档](https://docs.docker.com/)

[docker-compose](https://docs.docker.com/compose/)

> .gitignore文件对"docker-compose.yml"文件进行了忽略,目录就是为了差异环境使用不同的配置

1. 不同环境使用差异配置(不同环境下项目根目录的docker-compose.yml软连接到对应文件)

    - "docker/docker-compose.yml"开发环境使用配置
    - "docker/docker-compose.yml"生产环境使用配置
    
