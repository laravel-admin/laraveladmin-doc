# 执行生成命令

> 对laravel标准不熟悉的请读完后面的建表规则

> 开发过程中数据库中已创建好数据表,通过数据表生成迁移文件+模型+控制器+列表页面+编辑页面


```shell
php artisan create:all tests
```

# 生成结果

![编辑页面](https://www.laraveladmin.cn/storage/uploads/images/2020/12/05/5XrQuLoVNg6p450oLI12e7vLT7E0GdyAMByOnVp9.png)  
![列表页面](https://www.laraveladmin.cn/storage/uploads/images/2020/12/05/KEi0kZpKGW9cZUlYVnNT7KRuLdsGuLBCsbKpsFXj.png)


## 通过navicat工具快速创建数据表

### 建表规则

> 表名"`tests`"(laravel标准)

1. 主表使用复数形式
- 多对多关联中间表以单数形式且以字母顺序排列,例如:`menu_role`而不是`role_menu`

> 表名备注"`测试$softDeletes,timestamps,treeModel@belongsTo:user`"(备注名$数据表类型@数据表关联). [关联关系文档](https://laravel.geekai.co/post/22018)

1. 备注名:使用单条数据名称
- 数据表类型:
 - softDeletes:软删除功能,含有`deleted_at`字段.[软删除文档](https://laravel.geekai.co/post/1020)
 - timestamps:自动更新创建时间,修改时间;含有`created_at`,`updated_at`字段
 - treeModel:树状结构数据,含有字段`parent_id`,`level`,`left_margin`,`right_margin`
 - noId:多对多中间表没有主键id

> 表主键`id`

1. 主键ID统一使用id字段
 
> 字段规则

1. 所有字段能设置不能为空的尽量设置默认值
- 关联字段表名单数加"`_id`",如`user_id`

> 字段备注"`电子邮箱$email@sometimes|required|email|unique:tests,email`"(备注名$类型@验证规则)

1. 备注名:字段名称备注
- 类型:指定编辑页面的的编辑组件
- 验证规则:添加或修改数据时的验证([laravel字段验证规则](https://laravel.geekai.co/post/21984#toc-21))

![navicat建表](https://www.laraveladmin.cn/storage/uploads/images/2020/12/05/LBRiDlNhw30VR5Xpf9bX4NIOYdUwQPfx8BAVRdWe.png)

```sql
CREATE TABLE `tests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID$select2',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '名称',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '密码$password@sometimes|required|digits_between:6,18',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '电子邮箱$email@sometimes|required|email|unique:tests,email',
  `is_page` int(11) NOT NULL DEFAULT '0' COMMENT '是否为页面:0-否,1-是$radio@in:0,1',
  `status` tinyint(4) NOT NULL DEFAULT '2' COMMENT '状态:1-显示,2-不显示$select@in:1,2',
  `icons` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '图标$icon@nullable|alpha_dash',
  `method` int(11) NOT NULL DEFAULT '1' COMMENT '请求方式:1-get,2-post,4-put,8-delete,16-head,32-options,64-trace,128-connect$checkbox@required|array',
  `date_at` date DEFAULT NULL COMMENT '日期$date',
  `month_at` date DEFAULT NULL COMMENT '月份$month',
  `time` varchar(255) NOT NULL DEFAULT '' COMMENT '时间选择器$timeSelect',
  `time_picker` varchar(255) NOT NULL DEFAULT '' COMMENT '时间到秒$timePicker',
  `color` varchar(255) NOT NULL DEFAULT '' COMMENT '颜色选择器$color',
  `switch` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '开关$switch',
  `slider` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '滑块$slider',
  `rate` double(10,0) unsigned NOT NULL DEFAULT '0' COMMENT '评分星星$rate',
  `num` int(11) NOT NULL DEFAULT '0' COMMENT '数字$num',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述$textarea',
  `img` varchar(255) NOT NULL DEFAULT '' COMMENT '图片$upload',
  `ueditor` text COMMENT 'Markdown编辑器$markdown',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父级ID$ztree@nullable|exists:tests,id',
  `level` smallint(6) NOT NULL DEFAULT '0' COMMENT '层级',
  `left_margin` int(11) NOT NULL DEFAULT '0' COMMENT '左边界',
  `right_margin` int(11) NOT NULL DEFAULT '0' COMMENT '右边界',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tests_parent_id_index` (`parent_id`),
  KEY `tests_left_margin_index` (`left_margin`),
  KEY `tests_right_margin_index` (`right_margin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='测试$softDeletes,timestamps,treeModel@belongsTo:user';
```
