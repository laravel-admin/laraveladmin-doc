# 生成静态化index.html入口文件

#### 介绍

- 方便seo关键字搜索

- 入口直接走静态html不走php解析模块,响应更快

- 每次部署代码自动更新静态页面"public/index.html"

- 本地开发环境无需生成静态化index.html

- 命令对应源码文件"app/Console/Commands/BuildIndexHtml.php"

#### 生成代码命令

```shell
php artisan build:index.html --force
```

1. 生成代码位置"public/index.html"
