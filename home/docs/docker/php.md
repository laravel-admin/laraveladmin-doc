# php环境

> 基于php官方docker镜像进行编译

1. 环境源码"docker/php/Dockerfile"

2. 如需定制请修改Dockerfile源码,并修改docker-composer.yml文件

```yaml
  php: #用于开发环境
    build: ./docker/php
    #image: laraveladmin/laraveladmin #镜像源
```

3. 所有laravel项目代码都可直接挂载到"/var/www/laravel/"下第一级目录

    **容器将自动启动**
    
    - 队列
    - horizon队列监控
    - supervisord进程守护
    - 定时任务
    - 非开发环境启动swoole服务

4. 内置已安装composer程序

5. 容器环境自动挂载composer国内镜像配置文件"docker/php/config.json"

6. php官方镜像源相关文档:<https://registry.hub.docker.com/_/php>
