# mysql

> 考虑有可能多项目共用服务,我们将持久存储目录设计放在项目外部

1. 基于mysql官方镜像,镜像相关文档<https://registry.hub.docker.com/_/mysql>

2. mysql持久化数据目录位置"../docker/mysql/data"

3. 启动容器初始化sql命令"docker/mysql/init.sql",设置mysql对外可连接

4. 设置与php容器共享目录"../docker/mysql/shared",方便处理数据量大的导入导出数据
