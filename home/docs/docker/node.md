# node环境

1. 该环境主要用于前端编译打包使用,线上环境打包命令完成后予以退出关闭

2. 环境源码"docker/node/Dockerfile"

3. 内置国内镜像源`cnpm`命令

4. 如需定制请修改Dockerfile源码,并修改docker-composer.yml文件

```yaml
  node:
    build: ./docker/node
#    image: laraveladmin/node #镜像源
```

5. 执行前端资源编译命令

```shell
# 线上打包成压缩最小代码文件
docker-compose run --rm node npm run prod
# 开发环境开启实时编译监听
docker-compose run --rm node npm run watch
```
