# 模型

#### 介绍

我们需要实现下面的功能:

1. 通过数据表设置模型的白名单字段

2. 设置模型的隐藏字段

3. 设置日期字段

4. 设置模型中某些字典字段的映射说明

5. 控制器新建数据时字段的默认数据项显示

6. 控制器导出excel数据时的字段默认说明名称

7. 自动生成表与表的关联关系


#### 解决

1. 这里我们以users表为例

```shell
php artisan create:model users
```

2. 生成代码位置"app/Models"

3. 如果现有生成代码功能不满足于你,你可以自己定义生成代码模板"resources/views/php/model.blade.php"



```php

<?php

namespace App\Models;

use App\Facades\LifeData;
use App\Models\Traits\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasFactory, Notifiable,BaseModel,SoftDeletes,HasApiTokens;


    /**
     * The attributes that are mass assignable.
     * 通过读取数据库表生成白名单,将id进行排除
     * @var array
     */
    protected $fillable = [
        'uname',
        'password',
        'name',
        'email',
        'mobile_phone',
        'status',
        'description',
        'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'deleted_at'
        //'uname','email'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    /**
     * 字段值map
     * 该设置项将用于编辑页面的表单可选项,列表页面的筛选条件选项,excel导入导出数据转码解码操作
     * @var array
     */
    protected $fieldsShowMaps = [
        'status'=>[
            '注销',
            '有效',
            '停用'
        ]
    ];

    /**
     * 字段默认值
     * 编辑页面新增时的模型选项显示
     * @var array
     */
    protected $fieldsDefault = [
        'uname'=>'',
        'password'=>'',
        'name'=>'',
        'email'=>'',
        'mobile_phone'=>'',
        'remember_token'=>null,
        'status'=>1,
        'description'=>null,
        'avatar'=>''
    ];

    /**
     * 字段说明
     * 用于excel导出是的默认表头备注
     * @var array
     */
    protected $fieldsName = [
        'uname' => '用户名',
        'name' => '昵称',
        'avatar' => '头像',
        'email' => '电子邮箱',
        'mobile_phone' => '手机号码',
        //'remember_token' => '记住登录',
        'status' => '状态',
        'description' => '备注',
        //'created_at' => '创建时间',
        //'updated_at' => '修改时间',
        //'deleted_at' => '删除时间',
        'id' => 'ID',
    ];

    /**
     * 密码加密设置
     * @param  $value
     * @return  array
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value.'');
    }


    /**
     * 用户-后台用户
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function admin(){
        return $this->hasOne('App\Models\Admin');
    }

    /**
     * 用户操作日志
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logs(){
        return $this->hasMany('App\Models\Log');
    }

    /**
     * 三方登录用户
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ousers(){
        return $this->hasMany('App\Models\Ouser');
    }

    /**
     * 是否是Admin
     * @param $query
     * @return mixed
     */
    public function scopeIsAdmin($query){
        //当前生命周期内进行的缓存数据,正式环境运行是swoole中 该类数据不能存放在该类的属性中
        return LifeData::remember('_role_ids',function ()use($query){ 
            $user = Auth::user();
            return $user ? !!$user->admin : false;
        });
    }

    /**
     * 操作用户ID
     * @param $q
     * @return mixed
     */
    public function scopeGetOperateId($q){
        $main = Auth::user();
        return Arr::get($main,'id',0);
    }
}

```


