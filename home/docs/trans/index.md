# 多语言翻译

#### 配置

```dotenv
# 系统默认首选语言
APP_LOCALE=zh-CN
```

> 配置文件"config/laravel_admin.php"

```php
[
    'locales'=>['zh-CN','zh-TW','en'], //支持语言,系统默认首选语言必须包含在内
    'trans_prefix'=>'_trans_' //翻译语言前缀
];
```

#### 运行脚本自动辅助翻译

> 生成控制器,模型,页面等相关代码后可直接通过以下命令进行自动翻译修正相关代码

1. 先申请翻译配置,目前支持百度翻译，有道翻译，Google翻译(这里我们使用百度翻译API举例)
    - [百度翻译开发平台](https://fanyi-api.baidu.com/)
    - 申请开通"通用翻译API"
    - 在"开发者信息"中获取"APP ID"跟"密钥"配置到项目的.env中
        ```dotenv
           BAIDU_TRANSLATE_ID=
           BAIDU_TRANSLATE_KEY=
        ```
    - 在服务器地址中配置你的访问IP白名单([查看当前对外IP](https://qifu.baidu.com/?activeKey=SEARCH_IP))
    - 图片参考
        ![百度翻译示例](./百度翻译.png)
2. windows手动安装没有更新到翻译扩展包的可先执行以下命令安装翻译扩展包
```shell
composer install
```
3. 执行翻译命令
```shell
php artisan db:seed --class='Database\Seeders\Commands\Translation'
``` 

#### 翻译基础相关文档

1. [后台翻译文档](https://laravel.geekai.co/post/21988)
2. [前端页面翻译文档](https://kazupon.github.io/vue-i18n/zh/introduction.html)

#### 本系统翻译相关文档

> `重点注意翻译键中不要包含特殊符号点"."`

##### 后台新增自定义翻译函数"trans_path"

> 文件"/resources/lang/zh-CN/front.json"中的"_shared"内容为前后端共享的翻译数据

> laravel框架自带的翻译函数"trans"存在当没设置翻译项会出现前面路径信息,这样显示出来很尴尬,不够优雅

1. 使用示例

```php 
trans_path('Total number of back-end administrators','_shared.pages.admin')
//输出:后台管理员数
``` 
##### 前端新增自定义翻译函数"$tp"

> i18n库自带的翻译函数"$t"存在当没设置翻译项会出现前面路径信息,这样显示出来很尴尬,不够优雅

1. $t是翻译的是"/resources/lang/zh-CN/front.json"文件中最外层的数据项 

2. 使用示例

```javascript 
//前端模板中调用
{{$tp('Total number of back-end administrators',{'{lang_path}':'_shared.pages.admin','{lang_root}':''})}}
//输出:后台管理员数
```

3. 组件中设置全局路径

```vue
<template>
    <div>
        {{$tp('Total number of back-end administrators')}}
    </div>
</template>
<script>
    //输出 "后台管理员数"
    export default {
        data(){
              "{lang_path}":'_shared.pages.admin',
              '{lang_root}':''
        }
    }
</script>
```

4. 省略"{lang_root}"配置将默认到"pages"
```vue
<template>
    <div>
        {{$tp('username')}}
    </div>
</template>
<script>
    //输出 "用户昵称"
    export default {
        data(){
              "{lang_path}":'admin.users'
        }
    }
</script>
```

5. 表结构字段翻译需在"edit"组件或"datatable"组件的options参数设置翻译表名属性"lang_table"
```vue
<template>
    <div>
       <data-table :options="options"></data-table>
       <edit :options="options" ref="edit"></edit>
    </div>
</template>
<script>
    export default {
         components:{
            'data-table':()=>import(/* webpackChunkName: "common_components/datatable.vue" */ 'common_components/datatable.vue'),
            'edit':()=>import(/* webpackChunkName: "common_components/edit.vue" */ 'common_components/edit.vue'),
        },
        data(){
            options:{
                lang_table:"users"
            }
        }
    }
</script>
```
