# .env配置文件

> 配置文件位置".env"

> 执行环境安装前请务必设置好mysql密码,redis密码.使用docker环境安装如果忘记设置请清除../docker目录下是数据目录后再执行

> 重点注意有注释的相关配置项

```dotenv
#应用名称
APP_NAME=LaravelAdmin
#本地开发环境请设置成local  可选配置项:local-本地,testing-测试,staging-预上线,production-生产
APP_ENV=local
#通过php artisan key:generate命令会自动生成
APP_KEY=
#用于本地环境调试
APP_DEBUG=true
#域名设置(请不要用"/"结尾)
APP_URL=http://local.laraveladmin.cn

LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=db
DB_PORT=3306
DB_USERNAME=root
#数据库密码
DB_PASSWORD=admin123456
DB_DATABASE=laraveladmin

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=10080

REDIS_HOST=redis
#redis密码设置
REDIS_PASSWORD=admin123456
REDIS_PORT=6379

MAIL_MAILER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=null
MAIL_FROM_NAME="${APP_NAME}"

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

#应用名称简写
APP_NAME_SHORT="Ladmin"
#站点logo图片
APP_LOGO=/dist/img/logo.png
#阿里云发送短信设置
ALI_DAYU_SIGNATURE=
ALIYUN_SMS_AK=
ALIYUN_SMS_AS=
#注册用户短信模板代码
ALI_DAYU_CODE_REGISTER=
#手机短信登录短信模板代码
ALI_DAYU_CODE_LOGIN=
#忘记密码短信模板代码
ALI_DAYU_CODE_FORGOT=

#系统超管用户名
ADMIN_USER_NAME=admin
#系统超管用户密码,及系统通用密码初始值
ADMIN_PASSWORD=admin123456

#站点ICP备案号
ICP=
#正式环境swoole模式下设置,允许任何IP连接.本地开发环境设置为空可以不启动swoole提示效率
SWOOLE_HTTP_HOST=0.0.0.0
#正式环境swoole模式下访问服务端口
SWOOLE_HTTP_PORT=1515

#七牛云文件存储(图片等)配置项
QINIU_ACCESS_KEY=
QINIU_SECRET_KEY=
QINIU_DOMAIN_PUBLIC=
QINIU_BUCKET_PUBLIC=

#验证码类型:geetest-极验滑块验证,captcha-图形验证码(需手动安装扩展包mews/captcha)
APP_VERIFY_TYPE=geetest

#极验滑块验证码验证设置项
GEETEST_ID=
GEETEST_KEY=
#允许免验证码登录次数
APP_VERIFY_LOGIN_PASS_NUM=3
#禁用菜单目录设置(多渠道使用系统限制对应功能使用)
DISABLED_MENUS=
#页面所有请求模式:api或web
WEB_API_MODEL=api

#三方登录设置项
WEIBO_KEY=
WEIBO_SECRET=

QQ_KEY=
QQ_SECRET=

WEIXIN_KEY=
WEIXIN_SECRET=

WEIXINWEB_KEY=
WEIXINWEB_SECRET=

#是否单点登录
SINGLE_SIGN_ON=false

```
