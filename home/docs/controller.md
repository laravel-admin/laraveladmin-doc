# 控制器

#### 介绍

我们需要实现下面的功能:

1. 常用的增删改查

2. 列表查询排序

3. 进入页面设置默认选项

4. 条件过滤

5. 导入导出数据


#### 解决

上面都是常用功能也是编码比较花时间的工作量,来看看我们怎么实现的

1. 通过模型直接生成控制器,这里我们以后台的users表为例

```shell
php artisan create:controller Admin/User Models/User
```

2. 生成代码位置"app/Http/Controllers/Admin/"

3. 如果现有生成代码功能不满足于你,你可以自己定义生成代码模板"resources/views/php/controller.blade.php"

```php

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Traits\ResourceController;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    use ResourceController;

    /**
    * UserController constructor.
    */
    public function __construct()
    {
        $this->showIndexFieldsCount['logs'] = function($q){ //查询每个用户登录的次数
            $q->wehre('menu_id','=',64); 
        };
    }


    //默认排序
    protected $orderDefault = [ //顺序排序,排序不支持关联排序
        'updated_at' => 'desc',
        'id'=>'asc'
    ];

    /**
     * 资源模型
     * @var  string
     */
    protected $resourceModel = 'User'; //绑定的模型

    /**
     * 筛选过滤
     * @var array
     */
    protected $sizer = [
        'status' => '=', //状态筛选
        'created_at' => [ //创建时间筛选
            '>=',
            '<='
        ],
        'admin'=>'has', //关联筛选,是后台运营人员
        'admin.id'=>'=', //关联查询过滤
        'name|uname'=>'like', //名称或用户名关键字查找
        'mobile_phone|email' => 'like', //手机号或邮箱查找
    ];

   /**
     * 条件筛选查询默认查询是自动选中选项设置
     * @var array
     */
    protected $mapsWhereFields=[
        
    ];

    protected $keywordsMap=[ //前端列表页面的关键字搜索组选项框
        'name|uname'=>'用户',
        'mobile_phone|email'=>'联系方式'
    ];

    /**
     * 其它筛选条件输出
     * @var array
     */
    protected $otherSizerOutput = [
        '_key' => 'name|uname' //快捷搜索框默认使用用户进行搜索,对应keywordsMap的key
    ];


    /**
     * Index页面字段名称显示多条数据统计值
     * @var array
     */
    public $showIndexFieldsCount=[
        'logs', //查询所有操作日志总数
    ];

    /**
     * 指定列表页面返回的数据,可直接设置关联关系的字段
     * 关联关系获取时记得加上关联键字段才行
     * @var array
     */
    public $showIndexFields = [
        'id',
        'uname',
        'name',
        'mobile_phone',
        'email',
        'status',
        'created_at',
        'updated_at',
        'admin'=>[ //为空数组表示取全部字段

        ]
    ];

    /**
     * 编辑页面时的字段值
     * @var array
     */
    public $editFields = [
        'admin'=>[ //关联表
            'roles'=>[] //继续关联
        ] 
    ];

    /**
     * 导出字段名称定义,为空时将自动获取模型中的fieldsName属性进行使用
     * @var array 
     */
    public $exportFieldsName = [];

    /**
     * 添加或修改时数据验证规则
     * @return  array
     */
    protected function getValidateRule()
    {
        $id = Request::input('id',0);
        //没有密码值不对密码进行修改
        if(!Request::input('password')){
            Request::offsetUnset('password');
        }
        return [
            'uname' => 'sometimes|required|alpha_dash|between:5,18|unique:users,uname,'.$id.',id,deleted_at,NULL',
            'password' => ($id?'sometimes|':'').'required|between:6,18',
            'name' => 'required',
            'email' => 'sometimes|required|email|unique:users,email,'.$id.',id,deleted_at,NULL',
            'mobile_phone' => 'sometimes|required|mobile_phone|unique:users,mobile_phone,'.$id.',id,deleted_at,NULL',
            'status'=>'nullable|in:0,1,2'
        ];
    }


    /**
     * 对查询的数据进行前置过滤设置
     * 获取条件拼接对象
     * @return mixed
     */
    public function getWithOptionModel()
    {
        $this->bindModel OR $this->bindModel();
        $obj = $this->bindModel->with($this->selectWithFields())
            ->withCount(collect($this->getShowIndexFieldsCount())->filter(function($item,$key){
                return !is_array($item);
            })->toArray())
            ->whereDoesntHave('admin') //不显后台用户
            ->options($this->getOptions());
        return $obj;
    }

    /**
     * 提交修改前置处理
     * @param $data
     * @return mixed
     */
    protected function handlePostEditReturn(&$data){
        if(Arr::get($data,'id')==1){
            unset($data['status']);
        }
        return $data;
    }

    /**
     * 提交删除前置处理
     * @param $data
     * @return mixed
     */
    protected function handleDestroyReturn(&$data){
        $data = collect($data)->filter(function ($id){
            return $id>1;
        })->toArray();
        return $data;
    }





}

```


