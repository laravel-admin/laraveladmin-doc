# 自定义条件筛选

### 在更多筛选条件下添加本表中的字段'status'筛选

- 效果图

![筛选效果图](./筛选效果图.png)

- 控制器中定义属性

```php
    /**
     * 筛选过滤
     * @var array
     */
    protected $sizer = [
        'status' => '='
    ];
```

- 页面中设置显示

```html
<template slot="sizer-more" slot-scope="props">
    <div class="row" >
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 sizer-item">
            <select2 v-model="props.where['status']"
                     :default-options="props.maps['status']"
                     :placeholder-show="'状态'"
                     :placeholder-value="''"
                     :is-ajax="false" >
            </select2>
        </div>
    </div>
</template>
```

### 添加关联表中的字段'status'筛选(例如用admins列表中进行筛选users表中的状态)

- 控制器中定义属性

```php
    /**
     * 筛选过滤
     * @var array
     */
    protected $sizer = [
        'user.status' => '='
    ];
```

- 页面中设置显示

```html
<template slot="sizer-more" slot-scope="props">
    <div class="row" >
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 sizer-item">
            <select2 v-model="props.where['user.status']"
                     :default-options="props.maps['user.status']"
                     :placeholder-show="'状态'"
                     :placeholder-value="''"
                     :is-ajax="false" >
            </select2>
        </div>
    </div>
</template>
```

### 关键字搜索框自定义

- 控制器中定义属性

```php
    /**
     * 筛选过滤
     * @var array
     */
    protected $sizer = [
        'name|uname|mobile_phone|email' => '=' //通过用户姓名,账号,手机号,email信息进行搜索
    ];
```

- 页面中设置显示

![关键字设置](./关键字.jpg)

### 多组关键字搜索

- 控制器中定义属性

```php

    /**
     * 筛选条件设置
     * @var array
     */
    protected $sizer=[
        'user.name|user.uname'=>'like',
        'roles.name'=>'like',
    ];
    /**
     * 其它筛选条件输出
     * @var array
     */
    protected $otherSizerOutput = [
        '_key' => 'user.name|user.uname' //默认使用的关键字
    ];
    /**
     * 关键字搜索组
     * @var array
     */
    protected $keywordsMap=[
        'user.name|user.uname'=>'用户名称',
        'roles.name'=>'角色名称',
    ];
```

- 效果

![关键字分组效果图](./关键字分组.png)

### 进入页面默认筛选条件

- 控制器中定义属性

```php
    /**
     * 筛选条件默认值
     * @var array
     */
    protected $sizerDefault=[
        'status'=>1
    ];
```


