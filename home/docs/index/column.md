# 自定义列

### 定义列表页面组件的fields属性即可
```javascript
    export default {
        data(){
            return {
                options:{
                    fields: {
                        "id": {"name": "ID", "order": true},
                        "name": {"name": "名称", "order": true,levelName:'level',class:'text-left'},
                        "description": {"name": "描述", "order": true},
                        "parent.name": {"name": "父级名称", "order": true},
                        "admins_count": {"name": "管理员数量", "order": true},
                        //"created_at": {"name": "创建时间", "order": true},
                        "updated_at": {"name": "修改时间", "order": true},
                    },
                }
            };
        },
        computed:{

        },

    };
```

### 显示自定义

> 通过"props.k"判断对应列数据源

```html
<data-table class="box box-primary" :options="options">
    <template slot="col" slot-scope="props">
         <span v-if="props.field.type =='label'">
             <span class="label" :class="'label-'+statusClass[props.row[props.k]%statusClass.length]">
                 {{ props.data.maps[props.k] | array_get(props.row[props.k]) }}
             </span>
         </span>
         <div v-else-if="props.k =='name'" style="text-align: left">
             {{ props.row['level'] | deep}} {{props.row | array_get(props.k)}}
         </div>
         <span v-else-if="props.k =='admins_count'">
               <router-link :to="'/admin/admins?options='+getOptions({where:{'roles.id':props.row['id']}})">
                 {{props.row | array_get(props.k)}}
               </router-link>
         </span>
         <span v-else>
             {{props.row | array_get(props.k)}}
         </span>
     </template>
</data-table>
```
