# 自定义排序

### 设置默认排序
```php
   //默认排序
    protected $orderDefault = [
        'updated_at' => 'desc',
        'id'=>'asc'
    ];
```

### 页面中设置可操作排序

> 仅限于本表字段情况才能设置

![排序设置](./排序设置.png)
