# 列表顶部统计栏位自定义

> data-table组件中使用插槽

```html
<template slot="header" slot-scope="props">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 sizer-item">
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h4 class="text-center">{{props | array_get('data.list.total')}}元</h4>
                </div>
                <div class="small-box-footer">主营业务利润</div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 sizer-item">
            <div class="small-box bg-green" >
                <div class="inner" >
                    <h4 class="text-center">0.00元</h4>
                </div>
                <div class="small-box-footer">主营业务利润</div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 sizer-item">
            <div class="small-box bg-yellow" >
                <div class="inner" >
                    <h4 class="text-center">0.00元</h4>
                </div>
                <div class="small-box-footer">主营业务利润</div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 sizer-item">
            <div class="small-box bg-red" >
                <div class="inner" >
                    <h4 class="text-center">0.00元</h4>
                </div>
                <div class="small-box-footer">主营业务利润</div>
            </div>
        </div>
    </div>
</template>
```

> 效果

![顶部效果图](./顶部效果.png)
