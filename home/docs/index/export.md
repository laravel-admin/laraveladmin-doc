# 数据批量导出

> 控制器中定义属性

```php
  /**
     * excel导出数据获取字段
     * @var array
     */
    public $exportFields = [
        'id',
        'icons',
        'name',
        'url',
        'parent_id',
        'level',
        'method',
        'is_page',
        'status',
        'created_at',
        'updated_at',
        'parent' => ['name', 'id']
    ];

    //字段导出表头数据
    public $exportFieldsName = [
        'name' => '名称',
        'icons' => '图标',
        'description' => '描述',
        'url' => 'URL路径',
        'parent.name' => '父级名称',
        'method' => '请求方式',
        'is_page' => '是否为页面',
        'disabled' => '功能状态',
        'status' => '状态',
        'level' => '层级',
        'parent_id' => '父级ID',
        'id' => 'ID',
    ];
```


# 数据批量导入

> 导入数据格式请全部设置成文本格式进行导入

> 导入的数据表的sheet名称必须数据表表名一直进行模板校验防止数据导错

> 有主键值将直接对数据进行更新,没有进行创建

> 导入验证失败数据将再通过excel进行返回有问题的数据,错误信息输出在"error"字段中

### 自定义导入验证

```php
    /**
     * 导入验证规则
     * @return array
     */
    protected function getImportValidateRule($id=0,$item){
        return [];
    }
```

