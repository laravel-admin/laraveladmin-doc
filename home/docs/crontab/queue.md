# 异步队列

> 系统已默认设置发送异步消息的队列"send_message"

1. 短信服务我们这里使用的阿里大于三方服务

    - 注册用户申请相关配置[https://dayu.aliyun.com/](https://dayu.aliyun.com/)
    - 选择使用短信服务
    - 添加签名
    - 添加短信模板
    
    ```dotenv
    # 签名
    ALI_DAYU_SIGNATURE=
    # AccessKey ID
    ALIYUN_SMS_AK=
    # AccessKey Secret
    ALIYUN_SMS_AS=
    # 注册用户消息短信模板code
    ALI_DAYU_CODE_REGISTER=
    # 手机号直接登录消息短信模板code
    ALI_DAYU_CODE_LOGIN=
    # 忘记密码消息短信模板code
    ALI_DAYU_CODE_FORGOT=
    ```

2. 邮件通知配置示例
    
    - 相关消息通知模板在目录"resources/views/emails"
     
    ```dotenv
    MAIL_MAILER=smtp
    MAIL_HOST=smtp.qq.com
    MAIL_PORT=465
    MAIL_USERNAME=
    MAIL_PASSWORD=
    MAIL_ENCRYPTION=ssl
    ```

3. 使用docker环境搭建已经自动启动好了队列,并有supervisor程序进行守护队列进程

4. 队列相关文档[https://laravel.geekai.co/post/22009](https://laravel.geekai.co/post/22009)
    

