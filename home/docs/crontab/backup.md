# 数据库备份

> 数据备份插件包我们使用的"spatie/laravel-backup",更多数据备份相关文档请参考[https://github.com/spatie/laravel-backup](https://github.com/spatie/laravel-backup)

> 配置文件"config/backup.php"

#### 配置

```dotenv
# 发送备份情况通知邮箱地址
MAIL_FROM_ADDRESS=
# 发送备份情况通知邮箱名称
MAIL_FROM_NAME=
```

#### 定时任务自动备份

> 我们代码仓库使用的git仓库版本管理定时任务默认没有备份代码数据

> "app/Console/Kernel.php"中设置的非开发环境每天凌晨2点自动备份数据库任务

> 手动执行备份任务

```shell 
php artisan backup:run --only-db
```


