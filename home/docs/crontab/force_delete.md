# 自动清理软删除过期数据

> 针对含有软删除功能的数据表可能含有大量已删除的无用数据项问题,我们可以简单的在模型中设置过期时间长度让定时任务自动帮我们处理

#### 模型代码设置过期时长属性

```php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    //日志记录存放1年
    public $past_due = 3600*24*365;

}
```

#### 手动执行过期删除命令

```shell script
php artisan db:seed --class='Database\Seeders\Commands\ForceDeleteSeeder'
```


