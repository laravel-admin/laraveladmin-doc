# 接口文档

**需要对接系统的所有接口需要完成如下几步**

1. 通过"api_client"中间件认证(方便兼容移动端接口类似cookie的session id机制)
    
    - 没有"Client-Id"时,通过"/open/client-id"获取"Client-Id"并永久存储在客户端中
    - 后续的每个请求都需要在请求头中携带上"Client-Id"值
    - 类似支付回调的外部请求访问排除该中间件认证可在"ClientMiddleware"中间代码中添加排除路由
    ```php
    namespace App\Http\Middleware;
    
    use App\Facades\ClientAuth;
    use Closure;
    use Illuminate\Support\Facades\Response;
    
    class ClientMiddleware{
    
        public function __construct()
        {
            $this->except = [
                getRoutePrefix().'/open/config',
                getRoutePrefix().'/open/client-id'
            ];
    
        }
    
        protected $except = []; //排除路由
    }
    ```

2. 设置响应数据对应国家语言
    
    - 请求头中加入"Accept-Language"参数即可

3. 非get请求的注意csrf防护,相关文档<https://laravel.geekai.co/post/21972>
    - `api模式可不处理该项`
    - 请求头中携带"X-CSRF-TOKEN"
    - 可通过"/open/token"接口进行更新获取token值

4. 用户已登录信息
 
    - 请求头"Authorization"中携带登录成功后的token信息
    - 退出登录清空移动端存储的登录token数据

5. 跨域问题请求设置

    - 系统已自带支持跨域的扩展包"fruitcake/laravel-cors"
    - 参考其配置项即可"config/cors.php"

6. 请求传参提交数据或响应加解密

    - 通过
