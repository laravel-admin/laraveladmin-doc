# 滑块验证码

### 效果

![极验验证](./images/geetest.png)

### 注册申请配置

1. 注册地址[https://auth.geetest.com/register](https://auth.geetest.com/register) 

2. 申请行为验证

3. 配置.env

```dotenv
GEETEST_ID=
GEETEST_KEY=
```

### 使用

```html
<geetest 
:url="'/web-api/open/geetest'" 
v-model="verifyCode" 
:data="{
   client_fail_alert:'请正确完成验证码操作',
   lang:'zh-cn',
   product:'float',
   http:'http://'
}" 
class="geetest-code">
</geetest>
```
```javascript
    export default {
        components: {
            //滑块验证组件
            geetest:()=>import(/* webpackChunkName: "common_components/geetest.vue" */ 'common_components/geetest.vue'),
        },
       data() {
            return {
                verifyCode:'',
            }
    }
}
```
