# 图表组件

> 更多图表的options配置项请参照<a href="https://echarts.apache.org/examples/en/index.html" target="_blank">echarts官网</a>的DEMO

### 效果
![echart图表效果图](https://www.laraveladmin.cn/storage/uploads/images/2021/01/06/E5q989JODr5mKOKflx2WgkjWLyZ714KxAbqFgj1z.jpeg?)

### 编码
```html
<echart :options="options" style="height:400px;"></echart>
```
```javascript
    export default {
        props: {
        },
        components:{
            echart:()=>import(/* webpackChunkName: "common_components/echart.vue" */ 'common_components/echart.vue'),
        },
        data(){
            return {
                options:{
                    title: {
                        text: '柱状图'
                    },
                    tooltip: {},
                    legend: {
                        data:['销量']
                    },
                    xAxis: {
                        data: ["衬衫","羊毛衫","雪纺衫","裤子","高跟鞋","袜子"]
                    },
                    yAxis: {},
                    series: [{
                        name: '销量',
                        type: 'bar',
                        data: [5, 20, 36, 10, 10, 20]
                    }]
                }
               
            }
        }
    };
```

