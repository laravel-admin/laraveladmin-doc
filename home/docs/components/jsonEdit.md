# json编辑器

### 效果
![效果图](./images/json编辑器.png)

### 编码

```html
<json-edit
    v-model="value"
    :placeholder="'请输入,例如:{}'">
</json-edit>

<json-view :deep="3" style="height: 500px;overflow: scroll"  :closed="false" :theme="'one-dark'"  :data="value"/>

```

```javascript
    export default {
        components: {
            "json-edit": () => import(/* webpackChunkName: "common_components/jsonEdit.vue" */ 'common_components/jsonEdit.vue'),
            "json-view":()=>import(/* webpackChunkName: "vue-json-views/build/index.js" */ 'vue-json-views/build/index.js')
        },
        props: {},
        data() {
            return {
                value:{"user":"张三"}
            };
        }
    };
```
### 更多相关API
[https://zhaoxuhui1122.github.io/vue-json-view/](https://zhaoxuhui1122.github.io/vue-json-view/)

