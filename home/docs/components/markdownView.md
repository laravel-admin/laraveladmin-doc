# 颜色选择器

### 效果
![效果图](./images/markdown解析.png)

### 编码

```html
<markdown-view v-model="value">
</markdown-view>
```

```javascript
    export default {
        components: {
            markdownView:()=>import(/* webpackChunkName: "common_components/markdownView.vue" */ 'common_components/markdownView.vue'), //markdown编辑器
        },
        props: {},
        data() {
            return {
                value:'### 分页符 Page break \
\
> Print Test: Ctrl + P \
\
[========]\
\
### 绘制流程图 Flowchart\
\
```flow\
st=>start: 用户登陆\
op=>operation: 登陆操作\
cond=>condition: 登陆成功 Yes or No?\
e=>end: 进入后台\
\
st->op->cond\
cond(yes)->e\
cond(no)->op\
```\
\
[========]\
\
### 绘制序列图 Sequence Diagram\
\                   
```seq\
Andrew->China: Says Hello \
Note right of China: China thinks\nabout it \
China-->Andrew: How are you? \
Andrew->>China: I am good thanks! \
``` \
\
### End'
            };
        }
    };
```
### 更多相关API
[http://editor.md.ipandao.com/](http://editor.md.ipandao.com/)

