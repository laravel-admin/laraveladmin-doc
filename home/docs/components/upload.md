# 上传图片组件

### 效果
![效果图](./images/单图上传.jpg)
![效果图](./images/多图上传.jpg)

### 编码
```html
<upload :width="370" :height="201"  v-model="value">
</upload>
<upload v-model="values">
</upload>
```
```javascript
    export default {
        components: {
            "upload":()=>import(/* webpackChunkName: "common_components/upload.vue" */ 'common_components/upload.vue'),
        },
        props: {},
        data() {
            return {
                value:'',
                values:[]
            };
        }
    };
```

