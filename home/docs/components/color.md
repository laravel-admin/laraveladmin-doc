# 颜色选择器
### 效果
![echart图表效果图](./images/color.png)

### 编码
```html
<colorpicker v-model="value">
</colorpicker>
```
```javascript
    export default {
        components: {
            colorpicker:()=>import(/* webpackChunkName: "common_components/colorpicker.vue" */ 'common_components/colorpicker.vue'),
        },
        props: {},
        data() {
            return {
                value:''
            };
        }
    };
```
### 更多相关API
[https://itsjavi.com/bootstrap-colorpicker/](https://itsjavi.com/bootstrap-colorpicker/)

