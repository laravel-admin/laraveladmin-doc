# 去掉或变更动画

> "resources/js/pages/admin/layout.vue"中的如下标签去掉或变更样式即可

```html
<transition name="fade"
            mode="out-in"
            enter-active-class="animated lightSpeedIn faster"
            leave-active-class="animated lightSpeedOut faster">
    <router-view></router-view>
</transition>
```

> 参考vue动画文档

[VUE动画文档](https://cn.vuejs.org/v2/guide/transitions.html)
[动画样式库](https://animate.style/)
