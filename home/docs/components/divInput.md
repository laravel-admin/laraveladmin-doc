# 表单缩放组件

#### 效果图
![效果图](./images/表单缩放.png)

#### 编码

```html
<template>
    <div>
        <div-input ref="div_input" v-model="val" :zoom="100" :zoom-type="false" :unit="''">
            <template slot-scope="props">
                <el-input-number v-model="props.show"
                                 class="w-100"
                                 size="medium"
                                 @change="$refs['div_input'].enterVal(arguments[0])"
                                 :step="1">
                </el-input-number>
            </template>

        </div-input>
        ====
        {{val}}
    </div>
</template>
```
```javascript
export default {
    components: {
        elInputNumber:()=>import(/* webpackChunkName: "element-ui/lib/input-number" */ 'element-ui/lib/input-number'), //数字框异步组件
        "divInput": () => import(/* webpackChunkName: "common_components/divInput.vue" */ 'common_components/divInput.vue'),
    },
    props: {},
    data(){
        return {
            val: 10000,
        }
    },
};
```
