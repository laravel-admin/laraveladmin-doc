# 大文件分片上传

#### 效果图

![大文件上传效果图](./images/大文件上传.jpg)

#### 注册申请七牛云配置[七牛云官网](https://www.qiniu.com/)

```dotenv
QINIU_ACCESS_KEY=
QINIU_SECRET_KEY=
QINIU_DOMAIN_PUBLIC=
QINIU_BUCKET_PUBLIC=
```

#### 编码

```html
<qiniu-upload :token-url="'/open/qi-niu/token?disk=qiniu_public'"
    :root-path="'uploads/pdfs/'"
    :accept="'.pdf'"
    v-model="value">
</qiniu-upload>
```
```javascript
export default {
    components: {
        'qiniu-upload':()=>import(/* webpackChunkName: "common_components/qiniuUpload.vue" */ 'common_components/qiniuUpload.vue'),
    },
    data() {
        return {value:""}
    }
}
```

