# 颜色选择器

### 效果
![效果图](./images/markdown.png)

### 编码
```html
<editor-md v-model="value"
           placeholder="请输入内容"
           :disabled="false">
</editor-md>
```
```javascript
    export default {
        components: {
            editorMd:()=>import(/* webpackChunkName: "common_components/editorMd.vue" */ 'common_components/editorMd.vue'), //markdown编辑器
        },
        props: {},
        data() {
            return {
                value:''
            };
        }
    };
```
### 更多相关API
[http://editor.md.ipandao.com/](http://editor.md.ipandao.com/)

