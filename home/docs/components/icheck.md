# 单选,复选框

### 效果
![效果图](./images/单选,复选框.png)

### 编码

```html
  <icheck v-model="value" :option="1"> 复选框</icheck>
  <icheck v-model="value1" :option="1" type="radio" > 单选框</icheck>
```

```javascript
    export default {
        components: {
            "icheck": () => import(/* webpackChunkName: "common_components/icheck.vue" */ 'common_components/icheck.vue'),
        },
        props: {},
        data() {
            return {
                value:[],
                value1:1
            };
        }
    };
```
### 更多相关API
[http://icheck.fronteed.com/](http://icheck.fronteed.com/)

