# 百度坐标定位组件

### 效果
![效果图](./images/百度坐标定位.png)

### 编码
```html
  <div class="row">
    <div class="col-xs-6">
        坐标经度:{{row['lng'] || '--'}}
    </div>
    <div class="col-xs-6">
        坐标纬度:{{row['lat'] || '--'}}
    </div>
</div>
<input type="text" v-model="keywords">
<baidu-map v-model="row['location']"
           @change="row['lng']=row['location'][0];row['lat']=row['location'][1]"
           :keywords="keywords"
           :js-api-key="'tSSXXy6sVwoIAwCYCu2mumEJw5mD16Zd'"
></baidu-map>
```
```javascript
    export default {
        components:{
            "baidu-map":()=>import(/* webpackChunkName: "common_components/baiduMap.vue" */ 'common_components/baiduMap.vue'),
        },
        props: {},
        data() {
            return {
                row:{
                    lng:null,
                    lat:null,
                    location:null
                },
                keywords:'',
            };
        }
    };
```

