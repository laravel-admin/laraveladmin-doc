# 可查看密码输入框

### 效果
![效果图](./images/输入框锁.png)
![效果图](./images/输入框锁2.png)

### 编码
```html
<lock-edit v-model="value"></lock-edit>
```
```javascript
    export default {
        components: {
            'lock-edit':()=>import(/* webpackChunkName: "common_components/lockEdit" */ 'common_components/lockEdit'),
        },
        props: {},
        data() {
            return {
                value:''
            };
        }
    };
```

