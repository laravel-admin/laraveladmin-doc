# ztree组件

```html
<ztree v-model="parent_id"
       :check-enable="false"
       :multiple="false"
       :disabled="false"
       :id="'parent'"
       :chkbox-type='{ "Y" : "", "N" : "" }'
       :data="optional_parents">
</ztree>
```
```javascript
export default {
    components: {
        "ztree":()=>import(/* webpackChunkName: "common_components/ztree.vue" */ 'common_components/ztree.vue'),
    },
    data(){
        return {
            parent_id:0,
            optional_parents:[
                {id: 2, name: "运营", parent_id: 1},
                {id: 5, name: "后台主页", parent_id: 2},
                {id: 6, name: "用户管理", parent_id: 2}
            ]
        };
    }
}
```

> 更多详情设置请参考官网

[ztree官网](http://www.treejs.cn/v3/demo.php)

