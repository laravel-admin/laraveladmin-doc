# 下拉选项框

### 效果
![效果图](./images/下拉选项框1.png)
![效果图](./images/下拉选项框2.png)

### 编码
```html
<select2 v-model="value"
         :default-options="map"
         :placeholder-show="'状态'"
         :placeholder-value="''"
         :is-ajax="false" >
</select2>
```
```javascript
    export default {
        components: {
            "select2":()=>import(/* webpackChunkName: "common_components/select2.vue" */ 'common_components/select2.vue'),
        },
        props: {},
        data() {
            return {
                value:'',
                map:["注销","有效","停用"]
            };
        }
    };
```

