# tinymce富文本编辑器

#### 效果图

![富文本编辑器效果图](./images/富文本编辑器.jpg)

#### 申请注册获取配置项[tiny官网](https://www.tiny.cloud/)

![富文本编辑器KEY](./images/富文本编辑器KEY.jpg)

```dotenv
TINYMCE_KEY=
```

#### 编码

```html
<tinymce v-model="value">
</tinymce>
```
```javascript
export default {
    components: {
        'tinymce':()=>import(/* webpackChunkName: "common_components/tinymce.vue" */ 'common_components/tinymce.vue'),
    },
    data() {
        return {value:""}
    }
}
```
