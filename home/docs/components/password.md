# 可查看密码输入框

### 效果
![效果图](./images/密码输入框.png)
![效果图](./images/密码输入框2.png)

### 编码
```html
<password-edit v-model="value">
</password-edit>
```
```javascript
    export default {
        components: {
            passwordEdit: ()=>import(/* webpackChunkName: "common_components/passwordEdit.vue" */ 'common_components/passwordEdit.vue'),
        },
        props: {},
        data() {
            return {
                value:''
            };
        }
    };
```

