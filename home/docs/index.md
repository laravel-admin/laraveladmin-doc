# 列表页面

#### 生成命令

```shell
php artisan create:view Admin/User index
```

1. 生成代码位置"resources/js/pages/admin/users"

```html
<template>
    <div class="admin_user_index">
        <div class="row">
            <div class="col-xs-12">
                <data-table class="box box-primary" :options="options">
                </data-table>
            </div>
        </div>
    </div>
</template>

<script>
    import {mapState, mapActions, mapMutations, mapGetters} from 'vuex';

    export default {
        components:{
        },
        props: {
        },
        data(){
            let def_options = JSON.parse(this.$router.currentRoute.query.options || '{}');
            return {
                options:{
                    id:'data-table', //多个data-table同时使用时唯一标识
                    url:'', //数据表请求数据地址
                    operation:true, //操作列
                    checkbox:true, //选择列
                    btnSizerMore:true, //更多筛选条件按钮
                    keywordKey:'name|uname|mobile_phone|email', //关键字查询key
                    keywordGroup:false, //是否为选项组
                    keywordPlaceholder:'请输入用户昵称',
                    primaryKey:'id', //数据唯一性主键
                    defOptions:def_options, //默认筛选条件
                    fields: {
                        "uname": {"name": "用户名","order": true},
                        "name": {"name": "昵称", "order": true},
                        "mobile_phone": {"name": "手机号码", "order": true},
                        "email": {"name": "电子邮箱", "order": true},
                        "status": {"name": "状态", "order": true,type:'label'},
                    },
                }
            };
        },
        computed:{

        },

    };
</script>
<style lang="scss">


</style>

```

2. 后台返回数据注释说明,例如:"https://demo.laraveladmin.cn/web-api/admin/users"

```json5
{
    "list": { //数据源
        "current_page": 1,
        "data": [ //分页数据项
            { //每条数据项
                "id": 2,
                "uname": "test123456",
                "name": "张三",
                "mobile_phone": "13699411149",
                "email": "22@qq.com",
                "status": 1,
                "created_at": "2021-06-05 17:20:57",
                "updated_at": "2021-06-05 17:20:57"
            }
        ],
        "first_page_url": "http://demo.laraveladmin.cn/web-api/admin/users?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http://demo.laraveladmin.cn/web-api/admin/users?page=1",
        "links": [
            {
                "url": null,
                "label": "&laquo; 上一页",
                "active": false
            },
            {
                "url": "http://demo.laraveladmin.cn/web-api/admin/users?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": null,
                "label": "下一页 &raquo;",
                "active": false
            }
        ],
        "next_page_url": null,
        "path": "http://demo.laraveladmin.cn/web-api/admin/users",
        "per_page": 15, //每条多少条
        "prev_page_url": null,
        "to": 1,
        "total": 1 //总条数
    },
    "options": { //条件筛选+排序参数
        "where": { //条件筛选项
            "_key": "",
            "status": "",
            "created_at": [
                "",
                ""
            ],
            "admin": "",
            "name|uname": "",
            "name|uname|mobile_phone|email": "",
            "id": ""
        },
        "order": { //排序筛选项
            "updated_at": "desc",
            "id": "asc"
        }
    },
    "maps": { //字段对应值字典
        "status": [
            "Cancellation",
            "Effective",
            "Out of service"
        ]
    },
    "mapsRelations": [],//用于返回关系项对应的表名,便于处理设置的翻译
    "configUrl": { //操作权限数据项,为空表示没有对应操作权限
        "indexUrl": "/admin/users", //重置条件筛选请求地址,返回所有页面所需数据
        "listUrl": "/admin/users/list", //翻页请求地址,仅数据内容项
        "exportUrl": "/admin/users/excel", //导出数据请求地址
        "importUrl": "/admin/users/import", //导入数据请求地址
        "showUrl": "/admin/users/{id}", //详情数据接口
        "createUrl": "/admin/users", //新增数据请求地址
        "updateUrl": "/admin/users/{id}", //修改数据请求地址
        "deleteUrl": "/admin/users" //删除数据请求地址
    },
    "keywordsMap": { //关键字搜索组选项
        "name|uname|mobile_phone|email": "用户"
    },
    "excel": { //excel导出配置
        "sheet": "users", //sheet名称
        "fileName": "users", //导出对应的文件名
        "exportFields": { //导出的可选字段项
            "uname": "User name",
            "name": "Name",
            "avatar": "Head portrait",
            "email": "E-mail",
            "mobile_phone": "Phone number",
            "status": "State",
            "description": "Remarks",
            "id": "ID"
        }
    }
}
```

3. 后台判断操作是否有对应权限返回数据,前端通过返回数据判断对应操作按钮是否禁用或显示

```php
use App\Models\Menu;
$conditionDeleteUrl = '/admin/agent-systems/condition-delete'; //条件删除操作
$data['configUrl']['conditionDeleteUrl'] = Menu::hasPermission($conditionDeleteUrl, 'delete')?$conditionDeleteUrl:'';

```
