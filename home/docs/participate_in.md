# 参与贡献

`为了让大家拥有一个标准的通用后台管理系统欢迎大家参与进来`

**对您的好处**

1. 你在搭建一个全新的系统时可节省很多搭建开发成本

2. 你已使用该系统搭建的其它系统可迅速升级,增加一些通用功能(无需对比代码,拷贝代码到旧系统中)

    ```shell
    git pull laraveladmin master
    ```

3. 基于标准的master分支都是通用功能点,大家共同开发,从此你不是一个人在战斗!

4. 基于代码自动生成的一些规则,可以实现比较规范标准性的编码,整个团队将更便于理解,维护项目代码


**步骤**

1. 创建一个全新基于laraveladmin master分支的全新分支

2. 当你想修复一个公共bug,或添加一个通用新功能时,切换到该分支进行处理

3. 提交你修改的代码,并推送到仓库

4. 提交pr

5. 在其它分支上拉取分支代码

