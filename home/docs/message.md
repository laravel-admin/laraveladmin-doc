# 消息弹窗

1. 前端消息弹窗

```javascript
import {mapActions,mapMutations} from 'vuex';
export default {
    ...mapActions({
        pushMessage: 'pushMessage',
    }),
    //全局数据设置,设置弹窗提示
    ...mapMutations({
        set:'set'
    }),
    mounted() {
        //消息提醒
        this.pushMessage({
            'showClose':true,
            'title':'错误提示',
            'message':'',
            'type':'danger',
            'position':'top',
            'iconClass':'fa-warning',
            'customClass':'',
            'duration':3000,
            'show':true
        });
        //弹窗确认
        this.set({
            key:'modal',
            modal:{
                title:'提示',
                content: '这是弹窗',
                callback:()=>{
                    dd('点击确认')
                }}
        });
    }
}
```

2. 后台消息弹窗

```php
//控制器方法中返回
return Response::returns(['alert' => alert(['message' =>'错误信息'], 404)], 404);
```

```php
//控制器方法中返回
return Response::returns(['alert' => alert(['message' =>'成功信息'], 200)], 200);
```
