# 树状结构数据

#### 树状结构概念图

![树状结构概念图](./images/树状结构.png)

#### 如何设计使用树状结构模型

1. 数据库建表必须包含字段

```sql
CREATE TABLE `menus` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `parent_id` int NOT NULL DEFAULT '0' COMMENT '父级ID@sometimes|required|exists:menus,id',
  `level` smallint NOT NULL DEFAULT '0' COMMENT '层级',
  `left_margin` int NOT NULL DEFAULT '0' COMMENT '左边界',
  `right_margin` int NOT NULL DEFAULT '0' COMMENT '右边界',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menus_parent_id_index` (`parent_id`),
  KEY `menus_left_margin_index` (`left_margin`),
  KEY `menus_right_margin_index` (`right_margin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='菜单$softDeletes,timestamps,treeModel';
```

2. 模型中引入代码

```php
<?php
/**
 * 菜单模型
 */
namespace App\Models;
use App\Models\Traits\ExcludeTop; //排除顶层数据
use App\Models\Traits\TreeModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Traits\BaseModel;

class Menu extends Model
{
    //软删除,树状结构
    use SoftDeletes,TreeModel,BaseModel;
}
```

