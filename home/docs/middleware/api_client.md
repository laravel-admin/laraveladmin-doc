# api请求认证

### 客户端连接认证

> 预防恶意访问服务器接口

> api模式没有cookie机制不方便存储连接客户端数据,使用该方式模拟cookie机制

1. 中间件代码位置"app/Http/Middleware/ClientMiddleware.php"

2. 客户端没有"Client-Id"时,通过"/open/client-id"路由访问获取"Client-Id"

3. 客户端获取到"Client-Id"进行永久存储,并在后续的任何请求中的headers中添加该参数用于认证

### 用户登录认证

1. 通过提交登录成功接口返回的"_token"值进行保存

2. 后期的每个请求都将上面的"_token"值放入请求头的"Authorization"中


> 参考图片

![API认证参考图](../images/api认证.jpg)





