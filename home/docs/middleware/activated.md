# 注册用户后需要激活用户才能登录

> 系统默认注册用户功能需要填写手机号或者邮箱号码发送验证码进行确认手机号或者邮箱所属有效性,因此默认注册成功用户自动已经是激活状态

> 主要验证数据表users中的"status"字段

> 中间件代码位置"app/Http/Middleware/ActivatedMiddleware.php"
