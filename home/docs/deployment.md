# 线上服务器单机部署

#### 提交代码到远程仓库

1. 创建云端代码仓库(用码云为例)
    
    - 注册申请账号
    - 创建代码仓库
        ![创建仓库图片](./images/部署1.png)
        ![创建仓库图片](./images/部署2.png)
    
2. 进入项目代码根目录 

3. 将本地开发中的代码提交到自己的代码仓库

```shell
git init
git remote remove origin
git remote add origin https://用户名:密码@gitee.com/自己代码仓库.git
```

4. 使用git可视化工具提交推送本地代码

    - Windows使用:[tortoisegit](https://tortoisegit.org/)
    - mac使用:[sourcetree](https://www.sourcetreeapp.com/)

5. 修改代码提交后进行推送到远程仓库
    
   ```shell
    git push origin master
    ```
   
6. 登录线上服务器获取公钥

      - 执行以下命令后一直敲回车即可生成
      
        ```shell
        ssh-keygen -t rsa
        ```
            
      - 查看生成出来的公钥内容
          
        ```shell
        cat ~/.ssh/id_rsa.pub
        ```

7. 在码云代码仓库中设置部署上面生成的公钥内容
    ![公钥设置图片](./images/部署3.png)

#### 线上环境部署

1. 安装好git软件

2. 创建目录

```shell
mkdir /var/www
mkdir /var/www/laravel
cd /var/www/laravel
```

3. 下载你的项目代码这里使用ssh方式
![ssh地址图片](./images/部署4.png)
```shell
# 地址内容换成你自己的ssh部署地址
git clone https://gitee.com/zsping1989/laraveladmin-test.git
# 进入你的项目目录
cd laraveladmin-test
```
4. 参照.env.example配置[.env](env.md)文件(务必设置好mysql密码,redis密码)

- 数据库连接用户请使用root,程序需要检查数据库是否存在并创建数据库,开发环境的代码生成是通过读取数据表结构进行代码生成的
- 重点将配置APP_ENV设置成production
- .env中配置项值含有特殊符号的需要用'进行前后包裹

```shell
cp .env.example .env
vi .env
```

5. 初始化安装

```shell
sh ./docker/install.sh
```

> 注意检查"docker/mysql/init.sql"文件内容格式是否正确

```shell
cat docker/mysql/init.sql
```

```sql
GRANT REPLICATION SLAVE, REPLICATION CLIENT ON *.* TO 'root'@'%';
ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY '密码';
flush privileges;
```

```shell
docker-compose run --rm php composer global require laravel/envoy -vvv
docker-compose run --rm php composer global dump-autoload
docker-compose run --rm node cnpm install #前端编译扩展包安装
docker-compose run --rm php envoy run init --branch=master #项目初始化
docker-compose run --rm node npm run prod #编译前端页面js
docker-compose up -d #启动服务
```

6. 将域名解析到你的云主机IP

7. 代码更新

```shell
docker-compose exec php envoy run update
```

8. 重新编译前端资源

```shell
docker-compose run --rm node npm run prod
```







