# Markdown文档条款

1. 添加系统配置设置
    - "database/seeders/ConfigTableSeeder.php"文件中添加
        ```php
        
                Config::create([
                    'key' => 'privacy_policy',
                    'name' => '隐私政策',
                    'description' => '隐私政策条款内容',
                    'type'=>1,
                    'itype'=>3,
                    'value' => '# LaravelAdmin隐私政策
        #### 隐私政策
        隐私政策条款内容',
                ]);
        
        ```
    - "database/seeders/VersionSeeder.php"文件中添加,线上运行部署后可删除如下代码
        ```php
        
                    \App\Models\Config::firstOrCreate(['key'=>'privacy_policy'],[
            'key' => 'privacy_policy',
            'name' => '隐私政策',
            'description' => '隐私政策条款内容',
            'type'=>1,
            'itype'=>3,
            'value' => '# LaravelAdmin隐私政策
#### 隐私政策
隐私政策条款内容',
]);
        
        ```

2. 在配置页面"/admin/configs"中搜索"markdown_public_access_keys"设置对应的添加值的键"privacy_policy"
   
    ![树状结构概念图](./images/markdwon.png)

3. 访问"/open/markdown?key=privacy_policy"
