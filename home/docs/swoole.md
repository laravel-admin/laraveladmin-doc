# swoole

1. 系统集成的"swooletw/laravel-swoole"扩展包,相关文档可参考

    - [码云](https://gitee.com/zsping1989/laravel-swoole/wikis/pages)
    - [github](https://github.com/swooletw/laravel-swoole)

2. 相关配置项

    - "config/swoole_http.php"
    - "config/swoole_websocket.php"
    
3. 线上更新服务代码可使用如下命令进行更新

```shell
docker-compose restart php
```



