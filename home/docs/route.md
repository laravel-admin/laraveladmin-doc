# 路由

#### 介绍

传统的路由注册给我们带来的工作量还是比较大的,比如我们需要做这些:

1. 单页面应用前端需要注册路由

2. 后台应用需要注册路由

3. 菜单中需包含路由

4. 每当系统升级代码更新对应的菜单栏目需更新

5. 路由跟权限相关联

6. 接口相关文档需跟路由关联

#### 解决

> 具体路由注册添加直接在菜单页面中添加即可

**提示**

- 普通链接:系统外部链接或没有地址的分组归类菜单等(没有绑定系统内的某个控制器方法)
- 资源路由:绑定控制器的增删改查等方法,更多相关文档<https://laravel.geekai.co/post/21973>
- 单个路由:系统内当路由注册需绑定到控制器中的某个方法<https://laravel.geekai.co/post/19442>
- 菜单路由分词规则

    ```
    
    路由是:/admin/my-tests
    数据表是:my_tests
    控制器是:MyTestController
    模型是:MyTest
    页面是:my_tests/index.vue  my_tests/edit.vue
    
    ```

针对上面的问题我们进行了整合只需要简单维护好"routes/route.json"即可,该文件被前端和后端共同使用该文件进行注册

1. route.json设置
```json5
{
    "group": { //用于后台路由注册的group分组,相关属性可参考laravel的路由注册group
        "open": { //系统默认已经分好4个模块  none:空模块,open:未登录用户访问,home:登录用户访问,admin:后台运营人员访问
                "prefix": "/open", //路由前缀
                "namespace": "\\App\\Http\\Controllers\\Open", //命名空间
                "middleware": [] //中间件
            },
        }, 
    "menus": [ //普通单个路由注册
        {
            "id": 60, //请顺序填写,因为整个菜单是个数状结构数据,指定id为创建菜单顺序
            "name": "提交编辑修改密码", //路由名称
            "icons": "fa-lock", //路由图标,这个后台模板页面"/pages/UI/icons.html"进行选择设置
            "url":"/admin/personage/password", //路由地址
            "method":4, //请求方式参照Memu模型的字段映射:1-get,2-post,4-put,8-delete,16-head,32-options,64-trace,128-connect
            "status":2, //菜单是否显示(仅对页面路由设置有效)   状态:1-显示,2-不显示
            "is_page": 0, //是否为页面路由  页面路由将会在前端注册
            "parent_id": 59, //所属父级
            "disabled": 0, //是否未已禁用菜单,禁用菜单将完全无权限访问系统
            "description" : "提交编辑修改密码", //描述
            "group": "admin", //所属模块
            "action": "PersonageController@putPassword", //后台提供接口的控制器对应的方法
            //"env": "local", //仅本地开发环境才有的路由
        }
    ],
    "resource": [ //资源路由组成,将自动注册增删改查相关路由
       {
            "id": 9,
            "name": "普通用户",
            "icons": "fa-user",
            "url":"/admin/users",
            "method":1,
            "status":1,
            "is_page": 1,
            "parent_id": 6,
            "disabled": 0,
            "description" : "普通用户",
            "group": "admin",
            "options": {}, //可选择部分路由设置,比如{"only":["index","list"]}
            "end_id": 16, //结束ID
          //"env": "local", //仅本地开发环境才有的路由
        }
    ],
    "update_position": [ //菜单位置变动
      {"from": 83,"to": 99,"type": "update"}, //将菜单83移动到99的下面做子菜单 (必须满足99不能是83的子集)
      {"from": 83,"to": 99,"type": "after"}, //将菜单83移动到99的后面 (必须满足83,99的parent_id相同)
      {"from": 83,"to": 99,"type": "before"} //将菜单83移动到99的前面 (必须满足83,99的parent_id相同)
    ]
}
```

2. 变更路由后进入php容器执行命令生效菜单

```shell
php artisan db:seed --class=MenuTableSeeder
```

> "routes/route.json"数据将会在以下几个地方进行使用

- 1."routes/web.php"用于后端页面跟API路由注册
- 2."routes/api.php"用于API路由注册
- 3."resources/js/routes.js"前端页面路由注册
- 4."database/seeders/MenuTableSeeder.php"系统菜单数据填充进入数据表(每次执行系统升级时都会执行该填充)

#### 疑问

> 最新master分支代码已支持uuid作为主键,来解决多分支同步开发项目导致"route.json"容易冲突问题

1. `多人协作开发如何避免"/routes/route.json"冲突`

    - 方案一 : 大家都连接到同一份数据库
    - 方案二 :
        - 1. 开发者每次在需要添加菜单路由时,提前更新一下代码
        - 2. 如果"/routes/route.json"有变动执行迁移命令:`php artisan db:seed --class=MenuTableSeeder`
        - 3. 添加完菜单后及提交代码推送代码

2. `更新laraveladmin master分支后导致"/routes/route.json"冲突`

    - 1. 直接重置"/routes/route.json"文件
    - 2. 查看master分支的"/routes/route.json"新增了哪些路由功能,有需要的话手动添加进你的项目菜单即可








